# Name Sorter

This program is a solution for the **"Sorting Names"** assignment of the CPP Embedded Development Bootcamp.

The program's function is to read the contents of a file (**names.txt**), sort them in alphabetical order, and print the first 1000 sorted names.

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Notes](#notes)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

Requires `cmake` and `gcc`.

## Usage

	$ cd build
	$ cmake ../
	$ make
	$ ./main

## Notes

Currently the program only compares 2 random names from the first 1000 names of the **names.txt** list. The names are also printed entirely in lowercase.

## Maintainers

[Elmo Rohula @rootElmo](https://gitlab.com/rootElmo)

## Contributing

## License
