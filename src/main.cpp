#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <array>
#include <algorithm>
#include <time.h>

std::vector<std::string> names;
// std::vector<std::string> sorted_names;

/*Turn name to all lowercase for comparison*/
std::string to_lower(std::string input) {
	std::string data = input;
	std::for_each(data.begin(), data.end(), [](char &c) {
		c = ::tolower(c);
	});
	return data;
}

void sort() {

	std::array<std::string, 2> buffer;
	std::array<std::string, 2> sorted_names;
	srand(time(NULL));

	/*Pull two random names from the names vector*/
	for (int i=0; i<2; i++) {
		int name_index = rand() % 1000;
		std::string name_line = to_lower(names.at(name_index));
		buffer[i] = name_line;
	}

	/*Compare the pulled two names*/
	for (int j=0; j<buffer[0].length(); j++) {
		std::string first_name = buffer[0];
		std::string second_name = buffer[1];

		/*Typecast characters to int to compare them.
		This code assumes ASCII chatacters
		*/
		if ((int)first_name[j] > (int)second_name[j]) {
			sorted_names[0] = second_name;
			sorted_names[1] = first_name;
			break;
		} else if ((int)first_name[j] == (int)second_name[j]) {
			continue;
		} else {
			sorted_names[0] = first_name;
			sorted_names[1] = second_name;
			break;
		}
	}
	/*Print sorted names in order*/
	for (std::string print_str : sorted_names) {
		std::cout << print_str << "\n";
	}
}

std::ifstream file("../src/names.txt");
std::string line;

int main() {

	/*
	The names.txt couldn't be opened.
	Check that you are building the executeable to the build/
	folder. Also check, that the names.txt is present in
	the src/ folder.
	*/
	if (file.fail()) {
		std::cout << "Can't access file/file broken\n";
	}

	/*Read 1000 names to the names vector*/
	for (int i=0; i<1000; i++) {
		std::getline(file, line);
		names.push_back(line);
	}
	
	sort();

	file.close();
	return 0;
}
